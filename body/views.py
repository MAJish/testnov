import json

import requests
from django.http import JsonResponse

# Create your views here.
from django.views.decorators.csrf import requires_csrf_token, csrf_exempt
from telegram import KeyboardButton, ReplyKeyboardMarkup

from tgset.settings import TOKEN_BOT

url = f'secret'


@csrf_exempt
def send_message(request) -> JsonResponse:
    body = json.loads(request.body)
    key = KeyboardButton(text='Phone', request_contact=True)
    data = {
        "text": 'Нажми на кнопку, чтобы отправить номер.',
        "chat_id": body['message']['chat']['id'],
        "reply_markup": ReplyKeyboardMarkup(keyboard=[[key]], resize_keyboard=True).to_json()
    }
    if body['message'].get('contact'):
        send_nova(body['message']['contact']['phone_number'], body['message']['from']['username'])
        data['text'] = 'Спасибо.'
    if body['message']['message_id'] == 1:
        data["text"] = "Привет, а дай номер."
    response = requests.post(url+'sendMessage', data=data)
    return JsonResponse({"status": "ok"})


def send_nova(phone: str, login: str) -> None:
    """Метод для отправки данных пользователя в систему nova"""
    url_nova = 'https://s1-nova.ru/app/private_test_python/'
    data = {
        'phone': phone,
        'login': login
    }
    response = requests.post(url_nova, data=json.dumps(data))